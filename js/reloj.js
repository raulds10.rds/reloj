function obtenerHora(){
    let fecha = new Date();
   // console.log(fecha.getDate());
   //console.log(fecha.getDay());
   // console.log(fecha.getMonth());
   // console.log(fecha.getMinutes());
   // console.log(fecha.getSeconds());

    //traer etiquetas html
    let pDiaSemana = document.getElementById('diaSemana'),
        pDia = document.getElementById('dia'),
        pMes = document.getElementById('mes'),
        pAnio = document.getElementById('anio'),
        pHora = document.getElementById('horas'),
        pMinutos = document.getElementById('minutos'),
        pSegundos = document.getElementById('segundos'),
        pAmPm = document.getElementById('ampm');

        let meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
        let diaSemanas = ["Domingo", "Lunes", "Martes", "Miercoles", "jueves", "Viernes", "Sabado"]
        //asignar valores
        pDiaSemana.innerHTML = diaSemanas[fecha.getDay()];
        pDia.innerHTML = fecha.getDate();
        pMes.innerHTML = meses[fecha.getMonth()];
        pAnio.innerHTML = fecha.getFullYear();

        pHora.innerHTML = fecha.getHours();
        pMinutos.innerHTML = fecha.getMinutes();
        pSegundos.innerHTML = fecha.getSeconds();

        if(fecha.getHours() > 12){
            if((fecha.getHours() - 12) < 10){
                pHora.innerHTML = "0" + (fecha.getHours() - 12);
            }else{
                pHora.innerHTML = fecha.getHours() - 12;
            }
        }

        if(fecha.getHours() >= 12){
            pAmPm.innerHTML = "PM"
        }else{
            pAmPm.innerHTML = "AM"
        }

        if(fecha.getMinutes() < 10){
            pMinutos.innerHTML = "0" + fecha.getMinutes();
        }else{
            pMinutos.innerHTML = fecha.getMinutes();
        }

        if(fecha.getSeconds() < 10){
            pSegundos.innerHTML = "0" + fecha.getSeconds();
        }else{
            pSegundos.innerHTML = fecha.getSeconds();
        }

     


}

window.setInterval(obtenerHora,1000);